from fin1 import *
from fin2 import *
import sys


class MainWindow(QMainWindow):
    def __init__(self, parent=None):

        super(MainWindow, self).__init__(parent)
        self.prova="0"
        self.setGeometry(50, 50, 400, 450)
        self.finestra1 = Finestra1()
        self.finestra2 = Finestra2()
        self.startFinestra1()


    def startFinestra2(self):
        self.finestra2.setupFinestra2(self)
        self.finestra2.confBtn.clicked.connect(self.startFinestra1)
        self.show()

    def startFinestra1(self):
        self.finestra1.setupFinestra1(self)
        self.finestra1.btnConnetti.clicked.connect(self.salva)
        self.show()

    def salva(self):


        passaggio= self.finestra1.ipText.toPlainText()
        self.finestra2.passaggio=passaggio
        self.startFinestra2()



if __name__ == '__main__':
    app = QApplication(sys.argv)
    w = MainWindow()
    sys.exit(app.exec_())